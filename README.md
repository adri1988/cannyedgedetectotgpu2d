# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

GPU approach of the canny edge detection algorithm in "CUDA".

### How do I get set up? ###

*CUDA capable unix distribution
*clone or copy the code to a file with the .cu termination
*to compile "nvcc file.cu" -> ./a.out inputImage.bmp nameOfOutImage where input image MUST be .bmp file and nameOfOutputImage can be any name, this will create a .bmp file with this name.


### Who do I talk to? ###

* Repo owner or admin
